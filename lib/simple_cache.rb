# frozen_string_literal: true

class SimpleCache
  class << self
    attr_accessor :cache_dir
  end

  def initialize(key)
    raise 'No key provided' unless key
    raise 'SimpleCache.cache_dir is empty' unless self.class.cache_dir

    @path = File.join(self.class.cache_dir, key)
  end

  def read(format: nil)
    return nil unless cached?

    File.read path(format)
  end

  def write(content, format: nil)
    File.write path(format), content
  end

  def cached?(format: nil)
    File.exist? path(format)
  end

  private

  def path(format)
    "#{@path}.#{format}"
  end
end
