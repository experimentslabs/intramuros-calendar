# frozen_string_literal: true

require 'date'
require 'icalendar'
require 'json'

require_relative 'simple_cache'

class EventsFetcher
  attr_reader :uri, :file, :city_ids, :agglo_ids

  API_URL = 'https://api.appli-intramuros.com'

  def initialize(city_ids:, agglo_ids:)
    city_ids.sort!
    agglo_ids.sort!

    @city_ids   = city_ids
    @agglo_ids  = agglo_ids
    @uri        = URI("#{API_URL}/events/?city-id=#{city_ids.join(',')}&agglo-id=#{agglo_ids.join(',')}")
  end

  def entries
    @entries ||= cache.read(format: :json) || fetch
  end

  def ics
    @ics ||= cache.read(format: :ics) || create_calendar
  end

  private

  def cache
    @cache ||= SimpleCache.new "#{agglo_ids.join('-')}_#{city_ids.join('-')}_#{DateTime.now.strftime('%Y-%m-%d__%H')}"
  end

  def fetch
    response = Net::HTTP.get_response(@uri)

    raise "Failed to get the events list: API responded with #{response.code}" unless response.code == '200'

    cache.write(response.body, format: :json)
    JSON.parse response.body
  end

  def create_calendar
    # Use the time when the call was made as timestamp
    now      = Icalendar::Values::DateTime.new Time.now
    calendar = Icalendar::Calendar.new

    entries.each do |entry|
      create_event calendar, entry, now
    end

    calendar.publish

    ics = calendar.to_ical
    cache.write(ics, format: :ics)
    ics
  end

  def create_event(calendar, entry, date_stamp_value) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    calendar.event do |e|
      e.dtstart     = Icalendar::Values::Date.new Date.parse(entry['start_date'])
      e.dtend       = Icalendar::Values::Date.new(Date.parse(entry['end_date']) + 1.day)

      e.summary     = "[#{entry['city_name']}] - #{entry['title']}"
      e.description = entry['description'] if entry['description']
      e.url         = entry['shared_url'] if entry['shared_url']

      e.location    = entry['address_alabel'] if entry['address_alabel']
      e.geo         = entry['latitude'], entry['longitude'] if entry['latitude'] && entry['longitude']

      e.append_image entry['image'] if entry['image']

      e.dtstamp     = date_stamp_value
      e.uid         = "intramuros-event-#{entry['city_agglo']}-#{entry['city_agglo']}-#{entry['id']}"
      e.ip_class    = 'PUBLIC'
    end
  end
end
