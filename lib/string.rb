# frozen_string_literal: true

class Utils
  # Sanitize values in request parameters
  def self.sanitize_param(value)
    value.split(',').map do |v|
      v = v.to_i

      v.zero? ? nil : v
    end.compact.uniq
  end
end
