# Intramuros-calendar

> Proxy for [Intramuros](https://appli-intramuros.com) events, served as `ical` files.

Use it [online](https://intramuros-cal.experimentslabs.com)

## Why?

[Intramuros](https://appli-intramuros.com) is a great application but there is no way to get events in a `ical` format,
leading to a lot of mishaps:

- being forced to use the [website](https://intramuros.org/) to see the public city events
- being forced to use an app to see the public city events when the city has no _website_ version
- forgetting to open the app and missing
- sadness, anger, emotional damage

## Community and contributions

You are welcome to provide improvements and fixes to this project by [creating merge requests]() and [opening issues]().

You can reach the team on the [Matrix chatroom](https://matrix.to/#/!qpmxpfSIevwoRUgrxm:matrix.org?via=matrix.org) if
you want to discuss of this project or ExperimentsLabs' projects in general.

## How?

Intramuros-calendar is a small [Sinatra](https://sinatrarb.com) application that fetches and caches events data for an
hour, and spits out `ics` files. Download and use them "as-is" or add the link to your favourite calendar application.

## Usage

The easiest way to use the app is to use it online: find the link for the calendar you need, then use them in _whatever_
calendar client you use.

## Deployment

_Do as you usually do to deploy Sinatra apps..._

A systemd example unit file is available in this repository, as `intramuros-calendar.service`.

As an example:

```sh
# Create a user to run the site and prepare the files
sudo useradd -r -s /bin/false intramuros_cal
sudo mkdir /srv/intramuros-cal.servername.tld
sudo chown intramuros_cal:intramuros_cal /srv/intramuros-cal.servername.tld
cd /srv/intramuros-cal.servername.tld
sudo -u intramuros_cal git clone https://gitlab.com/experimentslabs/intramuros-calendar.git
sudo bundle install # Bad thing to do as root

# Create a system service
sudo cp intramuros-cal.service /etc/systemd/system
sudo vim /etc/systemd/system/intramuros-cal.service # Change paths and port in this file

# Configure Apache
sudo vim /etc/apache2/sites-available/intramuros-cal.servername.tld.conf # Create the proxypass
sudo a2ensite intramuros-cal.servername.tld.conf
sudo systemctl reload apache2
```

Example for an Apache Proxy configuration:

```config
<VirtualHost intramuros-cal.servername.tld:80>
    ServerName intramuros-cal.servername.tld

    # For reference, default Sinatra port is 4567.
    # 3001 is the port used in the systemd unit.
    ProxyPass / http://localhost:3001/
    ProxyPassReverse / http://localhost:3001/
</VirtualHost>
```

Getting a domain, creating subdomains and setting up SSL is beyond the scope of these notes.

## License

- Code for this project is licensed under the MIT license, available in the [LICENSE](LICENSE) file.
- Favicons are using [Calendar_1.png](https://commons.wikimedia.org/wiki/File:Calendar_1.png) from Wikimedia, and is
  licensed under
  the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
  licence.
- The meme used in the home page was generated on [https://imgflip.com](https://imgflip.com)
