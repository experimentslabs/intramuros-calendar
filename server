#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/https'
require 'sinatra'

require_relative 'lib/simple_cache'
require_relative 'lib/events_fetcher'
require_relative 'lib/string'

SimpleCache.cache_dir = File.join(__dir__, 'cache')

server_port = ENV.fetch('INTRAMUROS_CAL_PORT', nil)
set :port, server_port if server_port
set :public_folder, "#{__dir__}/static"

get '/' do
  cache_control :public
  send_file './static/index.html'
end

get '/cal', provides: [:ics] do
  %w[agglo_ids city_ids].each do |key|
    raise "#{key} is missing from parameters" unless params.key? key

    params[key] = Utils.sanitize_param params[key]
    raise "#{name} should be a comma-separated list of ids" if params[key].empty?
  end

  events = EventsFetcher.new city_ids: params['city_ids'], agglo_ids: params['agglo_ids']
  events.ics
end
